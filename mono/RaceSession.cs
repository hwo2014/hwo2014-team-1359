﻿using System;

namespace HWOLib
{
	public class RaceSession
	{
		public int Laps;
		public int MaxLapTimeMs;
		public bool QuickRace;
	}
}

