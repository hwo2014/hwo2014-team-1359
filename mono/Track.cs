﻿using System;

namespace HWOLib
{
	public class Track
	{
		public string ID;
		public string Name;
		public Piece[] Pieces;
		public Lane[] Lanes;
		public StartingPoint StartingPoint;

        public double GetTrackLength(int laneIndex)
        {
            double result = 0;
            foreach (var piece in Pieces)
                result += piece.GetLength(laneIndex);
            return result;
        }

        public int NextPieceIndex(int index)
        {
            index++;
            if (index > Pieces.Length - 1)
                index = 0;
            return index;
        }

        public double GetDistanceToNextSwitch(int pieceIndex, int laneIndex)
        {
            double len = 0;
            len += Pieces[pieceIndex].GetLength(laneIndex) / 2;
            pieceIndex = NextPieceIndex(pieceIndex);
            while (!Pieces[pieceIndex].Switch)
            {
                len += Pieces[pieceIndex].GetLength(laneIndex);
                pieceIndex = NextPieceIndex(pieceIndex);
            }
            len += Pieces[pieceIndex].GetLength(laneIndex) / 2;
            return len;
        }

        public void Analyze()
        {
            for(var i=0; i<Pieces.Length; i++)
            {
                if (Pieces[i].Switch)
                {
                    Pieces[i].SwitchLaneLengths = new double[Lanes.Length];
                    for (var j = 0; j < Lanes.Length; j++)
                    {
                        Pieces[i].SwitchLaneLengths[j] = GetDistanceToNextSwitch(i, j);
                    }
                }
            }
        }
	}

	public class StartingPoint
	{
		public Point Position;
		public double Angle;
	}

	public class Point
	{
		public double X;
		public double Y;
	}
}

