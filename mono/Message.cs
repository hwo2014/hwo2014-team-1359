﻿using System;

namespace HWOLib
{
	public abstract class Message
	{
		public string msgType;

		public abstract void Initialize ();
	}

	public class GameInit: Message
	{
		public GameInitData data;

		public override void Initialize ()
		{
			foreach (var piece in data.Race.Track.Pieces)
				piece.Track = data.Race.Track;
            foreach (var car in data.Race.Cars)
                car.Track = data.Race.Track;
            data.Race.Initialize();
		}
	}

	public class GameInitData
	{
		public Race Race;
	}

    public class CarPositions: Message
    {
        public CarPosition[] data;
        // public string GameId;
        // public int GameTick;

        public override void Initialize()
        {

        }
    }
}

