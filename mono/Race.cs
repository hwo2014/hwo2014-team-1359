﻿using System;
using System.Collections.Generic;

namespace HWOLib
{
	public class Race
	{
		public Track Track;
		public RaceSession RaceSession;
		public Car[] Cars;

        public Dictionary<string, Car> CarByID = new Dictionary<string, Car>();

        public void Initialize()
        {
            foreach (var car in Cars)
            {
                CarByID.Add(car.ID.Name, car);
            }
        }
	}
}

