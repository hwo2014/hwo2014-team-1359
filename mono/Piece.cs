﻿using System;

namespace HWOLib
{
	public abstract class Piece
	{
		public bool Switch;
			
		public Track Track;

        public double[] SwitchLaneLengths;

		public abstract double GetLength(int laneIndex);
	}

	public class StraightPiece : Piece
	{
		public double Length;

		public override double GetLength(int laneIndex)
		{
			return Length;
		}

        public override string ToString()
        {
            return string.Format("[StraightPiece]");
        }
	}

	public class BendPiece : Piece
	{
		public double Radius;
		public double Angle;

		public bool IsRightTurn
		{
			get {
				return Angle > 0;
			}
		}

        public double LaneRadius(int laneIndex)
        {
            var lane = Track.Lanes [laneIndex];
            if (Angle < 0)
            {
                return (Radius + lane.distanceFromCenter);
            }
            else
            {
                return (Radius - lane.distanceFromCenter);
            }
        }

		public override double GetLength(int laneIndex)
		{
            return LaneRadius(laneIndex) * Math.Abs(Angle) * Math.PI / 180.0;
		}

        public override string ToString()
        {
            return string.Format("[BendPiece: IsRightTurn={0}, Radius={1}, Angle={2}]", IsRightTurn, Radius, Angle);
        }
	}
}

