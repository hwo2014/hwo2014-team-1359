using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using HWOLib;

public class Bot {
    private string botName;
    private string botKey;

	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
            client.ReceiveTimeout = 60000;
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

            new Bot(reader, writer, new Join(botName, botKey));
            //new Bot(reader, writer, new CreateRace(botName, botKey, "usa", "", 1));
		}
	}

	private StreamWriter writer;

    private static GameInit game;

    Bot(StreamReader reader, StreamWriter writer, SendMsg join) {
		this.writer = writer;
		string line;

        if (join.GetType() == typeof(Join))
        {
            send(join);
        }
        else if (join.GetType() == typeof(CreateRace))
        {
            send(join);
            send(new JoinRace((CreateRace)join));
        } else {
            throw new Exception("wrong join");
        }

		while((line = reader.ReadLine()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			switch(msg.msgType) {
                case "carPositions":
                    var carpos = JsonConvert.DeserializeObject<CarPositions>(line);
                    foreach (var car in carpos.data)
                    {
                        game.data.Race.CarByID[car.ID.Name].Position = car;
                    }
                    // 0.5 ==> 300
                    // 0.6 ==> 360
                    // 0.7 ==> 420

                    // acc = 1.20 * delta(v)
                    // dec = 1.20 * delta(v)

                    // v = v0 + at = v0 + 1.2.(vthrottle - v).t
                    // x = v0.t + 1/2.a.t2
                    // 1. calc distance to next bend
                    var trg = 1.0;
                    var dist = game.data.Race.Cars[0].DistanceToNextBend(ref trg);
                    var distStr = game.data.Race.Cars[0].DistanceToNextStraight();
                    var newThrottle = 1.0;
                    if (dist > 0.001) // on straight
                    {
                        // dist -= 5;
                        // 2. simulate 0 throttle from now
                        var tck = 0;
                        var spd = game.data.Race.Cars[0].Velocity;
                        var dst = spd / 60; // 0
                        while (dst < dist && spd >= trg && tck < 500)
                        {
                            dst += spd / 60;
                            spd -= 1.2 * spd / 60;
                            tck++;
                        }
                        // 3. if target speed is met 1/60th before start go 0 throttle
                        if (dst > dist)
                            newThrottle = 0;
                        if (dst > dist && tck <= 2)
                        {
                            var nt = (trg - 0.98 * game.data.Race.Cars[0].Velocity) / 0.02;
                            Console.WriteLine("dot trg: " + trg.ToString() + " v: " + game.data.Race.Cars[0].Velocity.ToString() + " nt: " + nt.ToString() + " send: " + Math.Max(Math.Min(nt / 600, 1), 0).ToString());
                            newThrottle = Math.Max(Math.Min(nt / 600, 1), 0);
                        }
                    }
                    else
                    {
                        newThrottle = trg / 600;
                    }


                    //newThrottle = 0.7;
                    // Console.WriteLine(carpos.GameId + ":" + carpos.GameTick.ToString() + " - Speed: " + game.data.Race.Cars[0].Velocity.ToString() + " Acc: " + game.data.Race.Cars[0].Acceleration + " Angle: " + game.data.Race.Cars[0].Position.Angle.ToString() + " New throttle: " + newThrottle.ToString());
                    // Console.WriteLine("delta a: " + Math.Abs(game.data.Race.Cars[0].Acceleration - (newThrottle * 600 - game.data.Race.Cars[0].Velocity) * 1.2).ToString());

                    var cr = game.data.Race.Cars[0].CurrentBendRadius();
                    //Console.WriteLine("Radius: " + cr.ToString() + " AngleAcc: " + game.data.Race.Cars[0].AngleAcceleration.ToString() + " AngleVel: " + game.data.Race.Cars[0].AngleVelocity.ToString() + " Angle: " + game.data.Race.Cars[0].Position.Angle.ToString() + " Acc: " + game.data.Race.Cars[0].Acceleration.ToString() + " Velocity: " + game.data.Race.Cars[0].Velocity.ToString());
                    if (cr > 0.001)
                    {
                        var sp = (game.data.Race.Cars[0].Velocity / 60) * (game.data.Race.Cars[0].Velocity / 60) / cr;
                        if (sp > 0.32)
                        {
                            game.data.Race.Cars[0].Drifting = true;
                        }
                    }
                    if (game.data.Race.Cars[0].Drifting)
                    {
                        var force = 0.0;
                        if (cr > 0.001)
                        {
                            var vc = Math.Sqrt(0.32 * cr);
                            var v = game.data.Race.Cars[0].Velocity / 60;

                            force = 60.0 * v * vc * (v - vc) / 64.0 / cr;
                        }

                        //var dragforce = 0.38;
                        // dragforce *= - Math.Sin(Math.PI * game.data.Race.Cars[0].Position.Angle / 180.0);
                        //if (-Math.Sin(Math.PI * game.data.Race.Cars[0].Position.Angle / 180.0) < 0)
                        //    dragforce *= -1;

                        Console.WriteLine("Force  : " + force.ToString());

                        game.data.Race.Cars[0].CalculatedAngularAcceleration = force;

                        game.data.Race.Cars[0].CalculatedAngle += game.data.Race.Cars[0].CalculatedAngularVelocity;
                        game.data.Race.Cars[0].CalculatedAngularVelocity += game.data.Race.Cars[0].CalculatedAngularAcceleration;

                        //Console.WriteLine("CALC  : " + cr.ToString() + " CALC Acc: " + game.data.Race.Cars[0].CalculatedAngularAcceleration.ToString() + " CALC Vel: " + game.data.Race.Cars[0].CalculatedAngularVelocity.ToString() + " Angle: " + game.data.Race.Cars[0].CalculatedAngle.ToString());
                    }
                    //if (Math.Abs(game.data.Race.Cars[0].Velocity - trg) < 0.1)
                    //{
                        Console.WriteLine(game.data.Race.Cars[0].Position.Angle.ToString());
                    //}
                    // Console.WriteLine(game.data.Race.Cars[0].Velocity.ToString() + " Target: " + trg.ToString() + " NewThrottle: " + newThrottle.ToString());

                    if (cr > 0.001)
                    {
                        var vt = Math.Sqrt(0.32 * cr) * 60;
                        var sp = (game.data.Race.Cars[0].Velocity / 60) * (game.data.Race.Cars[0].Velocity / 60) / cr;
                        if (sp > 0.32)
                        {
                            //Console.WriteLine((sp - 0.32).ToString());
                            //newThrottle = game.data.Race.Cars[0].Velocity / 600; 

                            // angacc += 2.8 * overshoot
                            if (!game.data.Race.Cars[0].Drifting)
                            {
                                //game.data.Race.Cars[0].Drifting = true;
                                //game.data.Race.Cars[0].CalculatedAngularAcceleration = sp * Math.Cos(180.0 * game.data.Race.Cars[0].Position.Angle / Math.PI);
                            }
                        }
                        //newThrottle = (vt) / 600;
                        //if (game.data.Race.Cars[0].Velocity > trg)
                        //    newThrottle = 0;
                        //if (game.data.Race.Cars[0].Velocity < trg)
                        //    newThrottle = (game.data.Race.Cars[0].Velocity + (trg - game.data.Race.Cars[0].Velocity) / 1.15) / 600; // 1.1
                        if (newThrottle < 1 && Math.Abs(game.data.Race.Cars[0].Position.Angle) < 60) // 45
                        {
                            if ((game.data.Race.Cars[0].Position.Angle > 0 && game.data.Race.Cars[0].AngleVelocity < 0) ||
                                (game.data.Race.Cars[0].Position.Angle < 0 && game.data.Race.Cars[0].AngleVelocity > 0))
                            {
                                //Console.WriteLine("boost");
                                // newThrottle = Math.Min(newThrottle + 0.0007, 1); // for germany you can boost
                            }
                        }
                    }

                    // angacc *= (1 + c * acc)
                    // angacc *= (1 - sin ang)

                    SendMsg t;
                    // Console.WriteLine("Radius: " + cr.ToString() + " AngleAcc: " + game.data.Race.Cars[0].AngleAcceleration.ToString() + " AngleVel: " + game.data.Race.Cars[0].AngleVelocity.ToString() + " Angle: " + game.data.Race.Cars[0].Position.Angle.ToString() + " Acc: " + game.data.Race.Cars[0].Acceleration.ToString() + " Velocity: " + game.data.Race.Cars[0].Velocity.ToString() + " New throttle: " + newThrottle.ToString() + " (target: " + trg.ToString() + ", tt: " + (newThrottle * 600).ToString() + ")");

                    if (game.data.Race.Cars[0].SwitchNextPiece)
                    {
                        // determine left right...
                        if (game.data.Race.Cars[0].Position.PiecePosition.Lane.EndLaneIndex == 0)
                        {
                            t = new SwitchLane("Right");
                            Console.WriteLine("SWITCHING Right");
                        }
                        else
                        {
                            t = new SwitchLane("Left");
                            Console.WriteLine("SWITCHING Left");
                        }
                        game.data.Race.Cars[0].SwitchNextPiece = false;
                        //t = new Throttle(newThrottle); // REMOVE
                    }
                    else
                    {
                        // newThrottle = 0.5;
                        t = new Throttle(newThrottle);
                    }
                    send(t);

					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
                case "gameInit":
                    Console.WriteLine("Race init");
                    game = JsonConvert.DeserializeObject<GameInit>(line, new PieceConverter());
                    game.Initialize();
                    game.data.Race.Track.Analyze();
					send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
				default:
                    Console.WriteLine("Unknown Message Received: " + msg.msgType);
					send(new Ping());
					break;
			}
		}
	}

	private void send(SendMsg msg) {
        // Console.WriteLine(msg.ToJson());
		writer.WriteLine(msg.ToJson());
	}
}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

public abstract class JsonCreationConverter<T> : JsonConverter
{
    protected abstract T Create(Type objectType, JObject jsonObject);

    public override bool CanConvert(Type objectType)
    {
        return typeof(T).IsAssignableFrom(objectType);
    }

    public override object ReadJson(JsonReader reader, Type objectType, 
        object existingValue, JsonSerializer serializer)
    {
        var jsonObject = JObject.Load(reader);
        var target = Create(objectType, jsonObject);
        serializer.Populate(jsonObject.CreateReader(), target);
        return target;
    }

    public override void WriteJson(JsonWriter writer, object value, 
        JsonSerializer serializer)
    {
        throw new NotImplementedException();
    }
}

class PieceConverter: JsonCreationConverter<Piece>
{
    protected override Piece Create(Type objectType, JObject jsonObject)
    {
        if (jsonObject["length"] != null)
        {
            var result = new StraightPiece();
            result.Length = (double)jsonObject["length"];
            return result;
        }
        else
        {
            var result = new BendPiece();
            result.Angle = (double)jsonObject["angle"];
            result.Radius = (double)jsonObject["radius"];
            return result;
        }
    }
}

abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();

    public int gameTick;
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

    public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

class SwitchLane: SendMsg {
    public string value;

    public SwitchLane(string value) {
        this.value = value;
    }

    protected override Object MsgData() {
        return this.value;
    }

    protected override string MsgType() {
        return "switchLane";
    }
}

class BotID 
{
    public string name;
    public string key;
}

class CreateRace: SendMsg
{
    public CreateRace(string botName, string botKey, string trackName, string password, int carCount)
    {
        botId = new BotID { name = botName, key = botKey };
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    protected override string MsgType()
    {
        return "createRace";
    }

    public BotID botId;
    public string trackName;
    public string password;
    public int carCount;
}

class JoinRace: SendMsg
{
    public JoinRace(CreateRace createRace)
    {
        botId = new BotID { name = createRace.botId.name, key = createRace.botId.key };
        this.trackName = createRace.trackName;
        this.password = createRace.password;
        this.carCount = createRace.carCount;
    }

    protected override string MsgType()
    {
        return "joinRace";
    }

    public BotID botId;
    public string trackName;
    private string password;
    public int carCount;
}