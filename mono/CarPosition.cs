﻿using System;

namespace HWOLib
{
	public class CarPosition
	{
		public CarID ID;
		public double Angle;
        public Position PiecePosition;
	}

	public class Position
	{
		public int PieceIndex;
		public double InPieceDistance;
		public LanePosition Lane;
		public int Lap;
	}

	public class LanePosition
	{
		public int StartLaneIndex;
		public int EndLaneIndex;
	}
}

