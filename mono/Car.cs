﻿using System;

namespace HWOLib
{
	public class Car
	{
		public CarID ID;
		public CarDimensions Dimensions; 

        public int Lap = 1;

        private CarPosition position;
        public CarPosition Position
        {
            get {
                return position;
            }
            set {
                if (position != null)
                {
                    if (value.PiecePosition.Lane.StartLaneIndex != value.PiecePosition.Lane.EndLaneIndex)
                    {
                        Console.WriteLine("SWITCHED");
                    }
                    if (value.PiecePosition.PieceIndex != position.PiecePosition.PieceIndex && value.PiecePosition.PieceIndex == 0)
                    {
                        Lap++;
                    }
                    if (value.PiecePosition.PieceIndex == position.PiecePosition.PieceIndex)
                    {
                        Velocity = (value.PiecePosition.InPieceDistance - position.PiecePosition.InPieceDistance) * 60;
                    }
                    else
                    {
                        Console.WriteLine("Old piece: " + Track.Pieces[position.PiecePosition.PieceIndex].ToString());
                        Console.WriteLine("New piece: " + Track.Pieces[value.PiecePosition.PieceIndex].ToString());
                        // TODO not 4 lane ready
                        var nxtidx = Track.NextPieceIndex(value.PiecePosition.PieceIndex);
                        if (Track.Pieces[nxtidx].Switch && Track.Pieces[nxtidx].SwitchLaneLengths[value.PiecePosition.Lane.EndLaneIndex] > Track.Pieces[nxtidx].SwitchLaneLengths[1 - value.PiecePosition.Lane.EndLaneIndex])
                        {
                            SwitchNextPiece = true;
                        }
                        Velocity = (Track.Pieces[position.PiecePosition.PieceIndex].GetLength(position.PiecePosition.Lane.EndLaneIndex) - position.PiecePosition.InPieceDistance + value.PiecePosition.InPieceDistance) * 60;
                        Console.WriteLine("Velocity: " + Velocity.ToString());
                    }
                    AngleVelocity = (value.Angle - position.Angle);
                }
                position = value;
            }
        }

        private double velocity = -1;
        public double Velocity {
            get {
                return velocity;
            }
            set {
                if (velocity != -1)
                {
                    Acceleration = (value - velocity) * 60;
                }
                velocity = value;
            }
        }

        public double Acceleration;

        private double angleVelocity = -1;
        public double AngleVelocity
        {
            get {
                return angleVelocity;
            }
            set 
            {
                if (angleVelocity != -1)
                {
                    AngleAcceleration = (value - angleVelocity);
                }
                angleVelocity = value;
            }
        }

        public double AngleAcceleration;

        public Track Track;

        public bool SwitchNextPiece;

        public double DistanceToNextBend(ref double trg)
        {
            if (Track.Pieces[position.PiecePosition.PieceIndex].GetType() != typeof(BendPiece))
            {
                var dist = Track.Pieces[position.PiecePosition.PieceIndex].GetLength(position.PiecePosition.Lane.EndLaneIndex) - position.PiecePosition.InPieceDistance;
                var i = position.PiecePosition.PieceIndex + 1;
                if (i > Track.Pieces.Length - 1)
                    i = 0;
                while (Track.Pieces[i].GetType() == typeof(StraightPiece))
                {
                    dist += Track.Pieces[i].GetLength(position.PiecePosition.Lane.EndLaneIndex);
                    i = Track.NextPieceIndex(i);
                }
                if (i < position.PiecePosition.PieceIndex && Lap == 3)
                {
                    trg = 1200;
                }
                else
                {
                    trg = Math.Sqrt(0.32 * ((BendPiece)Track.Pieces[i]).LaneRadius(position.PiecePosition.Lane.EndLaneIndex)) * 60; // 0.65125;
                    switch ((int)((BendPiece)Track.Pieces[i]).LaneRadius(position.PiecePosition.Lane.EndLaneIndex))
                    {
                        case 40:
                            trg += 36;
                            break;
                        case 60:
                            trg += 36;
                            break;
                        case 90: 
                            trg += 69; // 72
                            break;
                        case 110:
                            trg += 72; // 72
                            break;
                        case 210:
                            trg = Math.Min(trg + 140, 600);
                            break;
                        case 190:
                            trg = Math.Min(trg + 140, 600);
                            break;
                        case 220:
                            trg = Math.Min(trg + 140, 600);
                            break;
                        default:
                            throw new Exception();

                    }
                }
                // finland
                var j = i;
                while (Track.Pieces[i] is BendPiece)
                {
                    i = Track.NextPieceIndex(i);
                }
                while (Track.Pieces[i] is StraightPiece)
                {
                    i = Track.NextPieceIndex(i);
                }
                if (i < j)
                {
                    trg = Math.Sqrt(0.32 * ((BendPiece)Track.Pieces[i]).LaneRadius(position.PiecePosition.Lane.EndLaneIndex)) * 60;
                }
                return dist;
            }
            else
            {
                trg = Math.Sqrt(0.32 * ((BendPiece)Track.Pieces[position.PiecePosition.PieceIndex]).LaneRadius(position.PiecePosition.Lane.EndLaneIndex)) * 60; // > 150 ? 1 : 0.65125;
                switch ((int)((BendPiece)Track.Pieces[position.PiecePosition.PieceIndex]).LaneRadius(position.PiecePosition.Lane.EndLaneIndex))
                {
                    case 40:
                        trg += 36;
                        break;
                    case 60:
                        trg += 36;
                        break;
                    case 90: 
                        trg += 69; // 72
                        break;
                    case 110:
                        trg += 72; // 72
                        break;
                    case 210:
                        trg = Math.Min(trg + 140, 600);
                        break;
                    case 190:
                        trg = Math.Min(trg + 140, 600);
                        break;
                    case 220:
                        trg = Math.Min(trg + 140, 600);
                        break;
                    default:
                        throw new Exception();

                }
                return 0;
            }
        }

        public double DistanceToNextStraight()
        {
            if (Track.Pieces[position.PiecePosition.PieceIndex].GetType() != typeof(StraightPiece))
            {
                var dist = Track.Pieces[position.PiecePosition.PieceIndex].GetLength(position.PiecePosition.Lane.EndLaneIndex) - position.PiecePosition.InPieceDistance;
                var i = position.PiecePosition.PieceIndex + 1;
                if (i > Track.Pieces.Length - 1)
                    i = 0;
                while (Track.Pieces[i].GetType() == typeof(BendPiece))
                {
                    dist += Track.Pieces[i].GetLength(position.PiecePosition.Lane.EndLaneIndex);
                    i++;
                    if (i > Track.Pieces.Length - 1)
                        i = 0;
                }
                return dist;
            }
            else
            {
                return 0;
            }
        }

        public double CurrentBendRadius()
        {
            if (Track.Pieces[position.PiecePosition.PieceIndex].GetType() != typeof(StraightPiece))
            {
                return ((BendPiece)Track.Pieces[position.PiecePosition.PieceIndex]).LaneRadius(position.PiecePosition.Lane.EndLaneIndex);
            }
            return 0;
        }

        public bool Drifting;
        public double CalculatedAngularAcceleration;
        public double CalculatedAngularVelocity;
        public double CalculatedAngle;
    }

	public class CarID
	{
		public string Name;
		public string Color;
	}

	public class CarDimensions
	{
		public double Width;
		public double Length;
		public double GuideFlagPosition;
	}
}

